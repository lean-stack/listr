import { db } from '$lib/firebase-admin/db';
import type { Submission } from '$lib/model/submission';
import type { Submitter } from '$lib/model/submitter';
import type { RequestHandler } from './__types/[list]';

export const get: RequestHandler = async ({ params }) => {

  const submissionsRef = db.collection(`lists/${params.list}/submissions`);
  const snapshot = await submissionsRef.get();
  const submissions = snapshot.docs.map(doc => ({ id: doc.id,  ...doc.data() } as Submission));

  return {
    body: submissions
  };
}

export const post: RequestHandler = async ({ params, request }) => {

  const submission: Submission = await request.json();

  const submissionsRef = db.collection(`lists/${params.list}/submissions`);
  let response = await submissionsRef.add(submission);
  submission.id = response.id;

  const submittersRef = db.collection(`lists/${params.list}/submitters`);
  const submitter: Submitter = { submissionId: submission.id };
  response = await submittersRef.add(submitter);
  submitter.id = response.id;

  return {
    body: {
      submission,
      submitter
    }
  }
}

export const put: RequestHandler = async ({ params, request }) => {

  const submission: Submission = await request.json();

  const submissionsRef = db.collection(`lists/${params.list}/submissions`);
  const res = await submissionsRef.doc(submission.id!).set(submission);
  return {};
}

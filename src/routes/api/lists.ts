import { db } from "$lib/firebase-admin/db";
import type { List } from "$lib/model/list";

export const get =  async () => {

  const listsRef = db.collection('lists');
  const snapshot = await listsRef.get();
  const lists = snapshot.docs.map(doc => ({ id: doc.id,  ...doc.data() } as List));

  return {
    body: lists
  };
}

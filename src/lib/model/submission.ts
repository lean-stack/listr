export type Submission = {
  id?: string;
  name: string;
  persons: string;
  what: string;
}

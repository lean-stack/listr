
import { initializeApp, cert, getApps, getApp } from "firebase-admin/app";

const appOptions = {
  credential: cert({
    clientEmail: import.meta.env.VITE_FB_ADMIN_CLIENT_EMAIL,
    privateKey: import.meta.env.VITE_FB_ADMIN_PRIVATE_KEY,
    projectId: import.meta.env.VITE_FB_PROJECT_ID
  })
}

export const app = !getApps().length ? initializeApp(appOptions) : getApp();
